var timer = null;
jQuery(document).ready(function () {
    var options = new primitives.orgdiagram.Config();

    var items = [];

    var rootItem = new primitives.orgdiagram.ItemConfig();
    rootItem.id = 0;
    rootItem.parent = null;
    rootItem.title = "CEO";
    rootItem.description = "Description A";
    rootItem.image = "samples/images/photos/a.png";
    items.push(rootItem);

    items.push(new primitives.orgdiagram.ItemConfig({
        id: 1,
        parent: 0,
        title: "Manag 1",
        description: "Manag Description",
        image: "samples/images/photos/a.png",
        itemType: primitives.orgdiagram.ItemType.Assistant,
        adviserPlacementType: primitives.common.AdviserPlacementType.Right,
        groupTitle: "Audit",
        groupTitleColor: primitives.common.Colors.Olive
    }));

    items.push(new primitives.orgdiagram.ItemConfig({
        id: 2,
        parent: 0,
        title: "Manag 2",
        description: "Manag Description",
        image: "samples/images/photos/b.png",
        itemType: primitives.orgdiagram.ItemType.Assistant,
        adviserPlacementType: primitives.common.AdviserPlacementType.Left,
        groupTitle: "Audit",
        groupTitleColor: primitives.common.Colors.Olive
    }));

    for (var index = 3; index <= 5; index ++) {
        
        items.push(new primitives.orgdiagram.ItemConfig({
            id: index,
            parent: 1,
            title: "Sub Adviser",
            description: "Sub Adviser Description",
            image: "samples/images/photos/s.png",
            itemType: primitives.orgdiagram.ItemType.SubAdviser,
            adviserPlacementType: primitives.common.AdviserPlacementType.Right,
            groupTitle: "Sub Adviser",
            groupTitleColor: primitives.common.Colors.Red
        }));
    }

    items.push(new primitives.orgdiagram.ItemConfig({
        id: 6,
        parent: 0,
        isVisible: false,
        title: "Aggregator",
        description: "Invisible aggregator",
        childrenPlacementType: primitives.common.ChildrenPlacementType.Horizontal
    }));


    items.push(new primitives.orgdiagram.ItemConfig({
        id: 7,
        parent: 6,
        title: "Manag 3",
        description: "Manag Description",
        image: "samples/images/photos/c.png",
        itemType: primitives.orgdiagram.ItemType.Assistant,
        adviserPlacementType: primitives.common.AdviserPlacementType.Right,
        groupTitle: "Audit",
        groupTitleColor: primitives.common.Colors.Olive
    }));


    items.push(new primitives.orgdiagram.ItemConfig({
        id: 8,
        parent: 6,
        title: "Manag 4",
        description: "Manag Description",
        image: "samples/images/photos/d.png",
        itemType: primitives.orgdiagram.ItemType.Assistant,
        adviserPlacementType: primitives.common.AdviserPlacementType.Left,
        groupTitle: "Audit",
        groupTitleColor: primitives.common.Colors.Olive
    }));

    items.push(new primitives.orgdiagram.ItemConfig({
        id: 9,
        parent: 6,
        isVisible: false,
        title: "Aggregator 2",
        description: "Invisible aggregator 2",
        childrenPlacementType: primitives.common.ChildrenPlacementType.Horizontal
    }));
    
    items.push(new primitives.orgdiagram.ItemConfig({
        id: 10,
        parent: 9,
        title: "Manag 5",
        description: "Manag Description",
        image: "samples/images/photos/d.png",
        itemType: primitives.orgdiagram.ItemType.Assistant,
        adviserPlacementType: primitives.common.AdviserPlacementType.Left,
        groupTitle: "Audit",
        groupTitleColor: primitives.common.Colors.Olive
    }));
    
    items.push(new primitives.orgdiagram.ItemConfig({
        id: 11,
        parent: 9,
        isVisible: false,
        title: "Manag 6",
        description: "Assistant Description",
        image: "samples/images/photos/d.png",
        itemType: primitives.orgdiagram.ItemType.Assistant,
        adviserPlacementType: primitives.common.AdviserPlacementType.Right,
        groupTitle: "Audit",
        groupTitleColor: primitives.common.Colors.Olive
    }));

    for (var index = 12; index <= 17; index += 1) {
        if (index == 15) {
            /* add invibsle item between chidlren */
            items.push(new primitives.orgdiagram.ItemConfig({
                id: 15,
                parent: 9,
                isVisible: false,
                title: "Aggregator 2",
                description: "Invisible aggregator 2",
                childrenPlacementType: primitives.common.ChildrenPlacementType.Matrix
            }));
        } else {
            items.push(new primitives.orgdiagram.ItemConfig({
                id: index,
                parent: 9,
                title: "Manag " + index,
                description: "Manag Description",
                image: "samples/images/photos/f.png",
                groupTitle: "Full Time"
            }));
        }
    }

    for (var index = 18; index <= 21; index ++) {
        
        items.push(new primitives.orgdiagram.ItemConfig({
            id: index,
            parent: 13,
            title: "Sub Adviser",
            description: "Sub Adviser Description",
            image: "samples/images/photos/s.png",
            groupTitle: "Dirctors",
            groupTitleColor: primitives.common.Colors.Red
        }));
    }
    
    for (var index = 22; index <= 25; index ++) {
        
        items.push(new primitives.orgdiagram.ItemConfig({
            id: index,
            parent: 16,
            title: "Sub Adviser",
            description: "Sub Adviser Description",
            image: "samples/images/photos/s.png",
            groupTitle: "Dirctors",
            groupTitleColor: primitives.common.Colors.Red
        }));
    }

    options.items = items;

    options.cursorItem = 0;
    options.normalLevelShift = 20;
    options.dotLevelShift = 10;
    options.lineLevelShift = 10;
    options.normalItemsInterval = 20;
    options.dotItemsInterval = 10;
    options.lineItemsInterval = 5;
    options.buttonsPanelSize = 48;
    options.hasButtons = primitives.common.Enabled.True;

    options.itemTitleSecondFontColor = primitives.common.Colors.White;

    options.pageFitMode = primitives.common.PageFitMode.Auto;
    options.graphicsType = primitives.common.GraphicsType.Auto;
    options.hasSelectorCheckbox = primitives.common.Enabled.True;
    options.hasButtons = primitives.common.Enabled.True;
    options.leavesPlacementType = primitives.common.ChildrenPlacementType.Matrix;

    options.arrowsDirection = primitives.common.GroupByType.Children;

    jQuery("#basicdiagram").orgDiagram(options);

    jQuery(window).on('resize', function (event) {
        onWindowResize();
    });
});

function onWindowResize() {
    if (timer == null) {
        timer = window.setTimeout(function () {
            jQuery("#basicdiagram").orgDiagram("update", primitives.orgdiagram.UpdateMode.Refresh);
            window.clearTimeout(timer);
            timer = null;
        }, 300);
    }
}