﻿/**
 * @preserve jQuery Widgets for Basic Primitives Diagram v5.1.0
 * Copyright (c) 2013 - 2018 Basic Primitives Inc
 *
 * Non-commercial - Free
 * http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Commercial and government licenses:
 * http://www.basicprimitives.com/pdf/license.pdf
 *
 */